# Article Extraction Endpoint



## About this endpoint

This endpoint extracts information from news websites like The Guardian, BBC News and many more.
You can extract titles, text, summary, keywords, authors, main image, all images, links, etc.

## Getting started

**Python code**
```python:
# make sure you have python3 and pip3 installed on your computer
# make sure you have requests library installed (pip3 install requests)
# get your api key from rapidapi

import requests


api_key = '***<insert your api key>***'


def print_article(article):
    line = '-' * 120
    print(line)
    print('title:', article.get('title'))
    print('authors:', article.get('authors'))
    print('keywords:', article.get('keywords'))
    print('main_image:', article.get('main_image'))
    print('summary:', article.get('summary'))
    print('text:', article.get('text'))
    print('images:', article.get('images'))
    print('links:', article.get('links'))
    print(line)


def get_article(news_website_url: str, api_key: str, language: str):
    url = 'https://text-analysis12.p.rapidapi.com/article-extraction/api/v1.3'
    params = {
        'language': language,
        'url': news_website_url
    }
    headers = {
        'x-rapidapi-key': api_key,
        'x-rapidapi-host': 'text-analysis12.p.rapidapi.com',
        "content-type": "application/x-www-form-urlencoded",
    }

    try:
        response = requests.post(url=url, headers=headers, data=params)
        result = response.json()

        if result.get('ok', False):
            article = result['article']
            return article
        else:
            print('error:', result.get('msg'))

    except Exception as e:
        print(f'error: {e}')
    return {}


news_website_url = 'https://www.bbc.com/news/56901183'
article1 = get_article(news_website_url, api_key, 'english')
print_article(article1)

news_website_url = 'https://www.theguardian.com/world/2022/jul/03/russias-claim-it-has-won-effective-control-of-luhansk-rejected-by-ukraine'
article2 = get_article(news_website_url, api_key, 'en') # en is same as english
print_article(article2)

news_website_url = 'https://www.aljazeera.net/encyclopedia/2022/7/3/%D9%84%D9%8A%D8%B3%D9%8A%D8%AA%D8%B4%D8%A7%D9%86%D8%B3%D9%83-%D8%A7%D9%84%D9%87%D8%AF%D9%81-%D8%A7%D9%84%D8%A3%D8%AE%D9%8A%D8%B1-%D9%84%D8%B1%D9%88%D8%B3%D9%8A%D8%A7'
article3 = get_article(news_website_url, api_key, 'arabic')
print_article(article3)

```

### Languages supported
**full-code, short-code**

- ukrainian, uk
- finnish, fi
- norwegian, no
- belarusian, be
- dutch, nl
- korean, ko
- slovenian, sl
- danish, da
- hindi, hi
- persian, fa
- italian, it
- portuguese pt
- chinese, zh
- spanish, es
- croatian, hr
- indonesian id
- turkish, tr
- russian, ru
- norwegian, nb
- serbian, sr
- macedonian mk
- estonian, et
- polish, pl
- greek, el
- japanese, ja
- arabic, ar
- french, fr
- romanian, ro
- swedish, sv
- hebrew, he
- bulgarian, bg
- english, en
- german, de
- vietnamese vi
- swahili, sw
- hungarian, hu

**You can use either the full-code or short-code to select the language.**


## Service Limits
- **rate limit: 100 requests per second**
